<?php

function sanitizeString($var)
{
    // Remove slashes
    $var = stripcslashes($var);

    // Delete HTML tags
    $var = strip_tags($var);

    // Turn reserved HTML characters into character entities
    $var = htmlentities($var);

    return $var;
}

/*
 * NOTE: To make this function work you need an open database connection
 */
function sanitizeMySQL($conn, $var)
{
    // Remove possible SQL injections
    $var = $conn->real_escape_string($var);

    // Remove potentially malicious HTML as well
    $var = sanitizeString($var);

    return $var;
}