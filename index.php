<?php

require_once 'include/dblogin.php';
require_once 'include/functions.php';

$htmlInjection = 'No string here yet...';

if (isset($_POST['htmlInjection']) && $_POST['htmlInjection'] != '')
{
    $htmlInjection = sanitizeString($_POST['htmlInjection']);
}

$conn = new mysqli($hn, $un, $pw, $db);
if ($conn->connect_errno)
{
    die($conn->connect_errno);
}

$mySqlInjection = 'No string here as well...';

if (isset($_POST['mySqlInjection']) && $_POST['mySqlInjection'] != '' && $conn)
{
    $mySqlInjection = sanitizeMySQL($conn, $_POST['mySqlInjection']);
}

$conn->close();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="post" action="index.php">
            Enter potential HTML injection:<br>
            <input type="text" name="htmlInjection"><br>
            Enter potential MySQL injection:<br>
            <input type="text" name="mySqlInjection"><br>
            <input type="submit"><br><br>
        </form>

        Here is your potential HTML injection once again, but neutralized:<br>
        <?=$htmlInjection;?><br><br>
        Here is your potential MySQL injection once again, but neutralized:<br>
        <?=$mySqlInjection;?>
    </body>
</html>